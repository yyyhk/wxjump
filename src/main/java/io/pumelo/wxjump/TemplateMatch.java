package io.pumelo.wxjump;



import java.util.Arrays;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

public class TemplateMatch {

    static class MatchResult{
        int x;
        int y;
        double score;

        public MatchResult(int x, int y, double score) {
            this.x = x;
            this.y = y;
            this.score = score;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public double getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }
    }

    public static MatchResult match(IplImage template,IplImage source){
        IplImage result = cvCreateImage(cvSize(source.width()-template.width()+1,
                source.height()-template.height()+1),IPL_DEPTH_32F,1);
        cvZero(result);
        cvMatchTemplate(source,template,result,CV_TM_CCOEFF_NORMED);
        double[] maxVal = new double[2];
        double[] minVal = new double[2];
        int[] maxLoc = new int[2];
        int[] minLoc = new int[2];

        cvMinMaxLoc(result,minVal,maxVal,minLoc,maxLoc,null);

        cvReleaseImage(result);
        System.out.println(Arrays.toString(maxVal));
        System.out.println(Arrays.toString(maxLoc));
        return new MatchResult(maxLoc[0],maxLoc[1],maxVal[0]);
    }
}
